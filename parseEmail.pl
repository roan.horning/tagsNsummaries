#!/usr/bin/perl
use strict;
use warnings;

=pod

=head1 DESCRIPTION

The purpose of this script is to produce a list of shows in a valid format 
for Dave Morriss' fix tags email import script. This script takes a plain text 
file as its input and prints to standard out a list of shows in the following 
format:

# <comment line> [optional]
show: <\d+> [required]
summary: <short description of the show> [optional, 100 characters max length]
tags: <comma seperated list of tags> [optional, 200 characters max length]
<blank line>

=head1 AUTHOR

Roan Horning	roan.horning<at>gmail.com

=head1 COPYRIGHT

Copyright 2021 Roan Horning. All rights reserved.

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

=cut

# Retrieve file provided in command arguements
my $fileName = shift @ARGV;

if (not defined $fileName) {
	die "Please provided a file to process.\n";
}

open(FH,"<$fileName") or die "Could not open $fileName file : $!\n";

my $shows = "";

# Consume the email into a string replacing line endings with space
while (my $line = <FH>) {
	# Trim white space from $line
	$line =~ s/^\s+|\s+$//g;
	chomp $line;
	$shows .= $line . " ";
}

close FH;

# Split $shows into an array containing each show's data
my $rs = "\o{36}"; # Ocatal value of ASCII Record Seperator (RS) character
$shows =~ s/show:/${rs}show:/g;
my @showList = split($rs, $shows);

foreach(@showList) {
	# Split each show into separate parts
	$_ =~ s/(#|[^#]show:|[^#]summary:|[^#]tags:)/${rs}$1/g;
	my @show = split($rs, $_);
	foreach(@show) {
		# Trim white space from item
		$_ =~ s/^\s+|\s+$//g;
	}
	$_ = \@show;
}
# Print out correctly formatted shows
foreach my $showOut (@showList) {
	foreach my $lineOut (@$showOut) {
		if ($lineOut ne '') {
			if ($lineOut =~ /^summary:/) {
				if (length($lineOut) > 100) {
					warn "\n###Warning: Summary longer than 100 characters ###\n";
				}
			}
			elsif ($lineOut =~ /^tags:/) {
				# Trim white space after comma
				$lineOut =~ s/,\s+/,/g;
				# Put back space after comma in quoted tags
				$lineOut =~ s/(\"[^,]+,)([^\"]+)/$1 $2/g;
				if (length($lineOut) > 200) {
					warn "\n### Warning: Tags longer than 200 characters ###\n";
				}
			}
			print "$lineOut\n";
		}
	}
	print "\n";
}
