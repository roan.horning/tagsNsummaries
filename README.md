The purpose of this script is to produce a list of shows in a valid format 
for Dave Morriss' fix tags email import script. <br>This script takes a plain text 
file as its input and prints to standard out a list of shows in the following 
format:

\# <comment line> [optional] <br>
show: \<\\d+\> [required] <br>
summary: \<short description of the show\> [optional, 100 characters max length] <br>
tags: \<comma seperated list of tags\> [optional, 200 characters max length] <br>
\<blank line\>
